module readInData
  use global
  use random

  implicit none

  contains

  subroutine readData(input, dataIn)
    use,intrinsic::IEEE_ARITHMETIC
    type(varInput):: input
    type(dataInput)::dataIn

    character(len=1000):: temp
    integer::IOstatus, maxSize
    integer:: counting, nRow
    integer:: i,j,k, l
    logical::test
    integer, dimension(:,:), allocatable::geno, temp_geno
    character(len=5), dimension(:, :), allocatable:: sel_log
    integer, dimension(:), allocatable:: sel_id



    IOstatus = 0
    !Loading in the CovInd file
    if (TLC(input%covIndFile).ne."none") then

      call readInFileReal(dataIn%covInd, input%covIndFile)
      if (size(dataIn%covInd(:,1))>input%maxSize) then
        input%maxSize = size(dataIn%covInd(:,1))
      end if
      if (allocated(input%cov)) then
        deallocate(input%cov)
      end if
      allocate(input%cov(input%maxSize-1))
      input%cov = -9
      input%varCovInd = -9
    end if

    call ReadInFileInteger(dataIn%geno, input%genoFile)

    if (TLC(input%covLocFile) .ne. "none") then
      call ReadInFileReal(dataIn%covLoc, input%covLocFile)
      if (size(dataIn%covLoc(1,:)) .ne. size(dataIn%geno(:,1))-1) then
        write(*,*) "The number of Loci for the variation is different to the number of Loci given"
        write(*,*) "Num Loci Var: " , size(dataIn%covLoc(1,:)), size(dataIn%geno(:, 1))
        stop
      end if
    end if

    if (TLC(input%selFile) .ne. "none") then
      call ReadInFileInteger(dataIn%sel, input%selFile)
      do i = 1, size(dataIn%sel(1,:))
        do j = 2, size(dataIn%sel(:,1))
          if ((dataIn%sel(j,i) .ne. 1) .and. (datain%sel(j,i).ne. 0) ) then
            write(*,*) "I'm sorry - for selection choose 1 for true and 0 for false."
            stop
          end if
        end do
      end do
    end if

  end subroutine readData

  subroutine MakeArray(dataIn, input)
    implicit none
    type(varInput)::input
    type(dataInput)::dataIn
    integer::i
    if (.not. allocated(dataIn%geno)) then
      write(*, *) "Geno not allocated - error. ERROR!"
      stop
    end if

    if (.not. allocated(dataIn%sel)) then
      allocate(dataIn%sel(size(dataIn%geno(1,:)), input%maxSize))
      dataIn%sel = 1
    end if

    if (.not. allocated(dataIn%covInd)) then
      allocate(dataIn%covInd(input%maxSize, size(dataIn%geno(1, :))))
      write(*,*) input%maxSize,  "INPUT"
      dataIn%covInd(1,:) = dataIn%geno(1,:)
      call random_normal_distribution(dataIn%covInd, input%cov, input%varCovInd)
    end if

    if (.not. allocated(dataIn%covLoc)) then
      allocate(dataIn%covLoc(input%maxSize, size(dataIn%geno( :,1))))
      !dataIn%covLoc(1,:) = dataIn%geno(:,1)
      do i = 1, size(dataIn%geno(:,1))
        dataIn%covLoc(1,i) = i
      end do
      call random_gamma_distribution(dataIn%covLoc, input%varCovLoc)
    end if
  end subroutine MakeArray

  subroutine random_normal_distribution(arrayIn, meanIn, stdevIn)
    real, allocatable, dimension(:,:):: arrayIn
    double precision, allocatable, dimension(:)::meanIn, stdevIn

    integer::mean, stdev
    integer::i,j

    do i = 2, size(arrayIn(:,1))
      do j = 1, size(arrayIn(1,:))
    if (size(meanIn)==1) then
      mean = meanIn(1)
    else
      mean = meanIn(j-1)
    end if
    if (size(stdevIn)==1) then
      stdev = stdevIn(1)
    else
      stdev = stdevIn(j-1)
    end if

        arrayIn( i, j) = stdev*random_normal()+mean
      end do
    end do
    

    end subroutine random_normal_distribution

    subroutine random_gamma_distribution(arrayIn, covLoc)
      real, allocatable, dimension(:,:), intent(inout):: arrayIn
      double precision, allocatable, dimension(:):: covLoc
      real:: shap, scal, temp

      integer::i, j


      if ((size(covLoc) .ne. size(arrayIn(:,1))-1).and. (size(covLoc).ne. 1)) then
        write(*,*) "There is a problem with the size of the covLoc array"
        write(*,*) size(covLoc), size(arrayIn(:,1))
      end if
      
      do j = 2, size(arrayIn(:,1))
        if (size(covLoc)==1) then
          shap = covLoc(1)
        else
          shap = covLoc(j-1)
        end if
        do i = 1, size(arrayIn( 1,:))
          scal = 1/shap
          arrayIn(j, i)= scal*random_gamma(shap, .true.)
        end do
      end do
    
    end subroutine random_gamma_distribution




  subroutine testData(dataIn, input)
    implicit none
    type(dataInput), intent(inout)::dataIn
    type(varInput)::input

    integer, allocatable, dimension(:)::sel, loc, ind, gen, passed
    integer, allocatable, dimension(:,:):: sel_values, geno_values
    real, allocatable, dimension(:,:):: loc_values, ind_values
    integer::nLoc, test, num_indiv, i, j

    logical:: seltest, loctest, indtest
    nLoc = size(dataIn%geno(1,:))

    allocate(sel(size(dataIn%sel(1,:))))
    allocate(loc(size(dataIn%covLoc(1,:))))
    allocate(ind(size(dataIn%covInd(1,:))))
    allocate(gen(size(dataIn%geno(1,:))))
    allocate(passed(size(dataIn%geno(1,:))))

    sel = dataIn%sel(1,:)
    loc = dataIn%covLoc(1,:)
    ind = dataIn%covInd(1,:)
    gen = dataIn%geno(1,:)


    num_indiv=0
    passed = -9
    do i =1, size(dataIn%geno(1,:))
      seltest=.false.
      loctest = .false.
      indtest = .false.
      test = gen(i)
      do j = 1, size(sel)
        if (test .eq. sel(j)) then
          seltest = .true.
        end if
      end do
!      do j = 1, size(loc)
!        if (test .eq. loc(j)) then
!          loctest = .true.
!        end if
!      end do
      do j = 1, size(ind)
        if (test .eq. ind(j)) then
          indtest = .true.
        end if
      end do
      if (indtest .and. seltest) then
        num_indiv = num_indiv+1
        passed(num_indiv) = test
      else
        write(*,"(A,I0,A,L2,A,L2,A,L2)") "Individual ", test, "doesn't have ind and sek and has been dropped. Sel: ", seltest, " Ind: ", indtest
      end if
    end do

    allocate(sel_values(size(dataIn%sel(:,1)), size(dataIn%sel(1,:))))
    sel_values = dataIn%sel

    deallocate(dataIn%sel)
    allocate(dataIn%sel(size(sel_values(:,1)),num_indiv ))
     
    do j = 1, num_indiv
      do i =1, size(sel_values(:,1))
        if (passed(j) == sel_values(1, i)) then
          dataIn%sel(:,j) = sel_values(:,i)
        end if
      end do
    end do
    deallocate(sel_values)
    
    allocate(geno_values(size(dataIn%geno(:,1)), size(dataIn%geno(1,:))))
    geno_values = dataIn%geno
    write(*,*) size(geno_values)

    deallocate(dataIn%geno)
    allocate(dataIn%geno( size(geno_values(:,1)), num_indiv))
    do j = 1, num_indiv
      do i =1, size(geno_values(1,:))
        if (passed(j) == geno_values(1, i)) then
          dataIn%geno(:, j) = geno_values(:, i)
        end if
      end do
    end do
    deallocate(geno_values)

!    allocate(loc_values(size(dataIn%covLoc(:,1)), size(dataIn%covLoc(1,:))))
!    loc_values = dataIn%covLoc
!
!    write(*,*) "HDKJ", loc_values(:,1)
!
!    deallocate(dataIn%covLoc)
!    allocate(dataIn%covLoc(size(loc_values(:,1)),num_indiv ))
!      
!    do j = 1, num_indiv
!      do i =1, size(loc_values(1, :))
!        if (abs(passed(j) - loc_values(1, i))<0.001) then
!          write(*,*) "HFJDF", passed(j), loc_values(:,i)
!          dataIn%covLoc(:,j) = loc_values( :,i)
!        end if
!      end do
!    end do
!
    allocate(ind_values(size(dataIn%covInd(:,1)), size(dataIn%covInd(1,:))))
    ind_values = dataIn%covInd

    deallocate(dataIn%covInd)
    allocate(dataIn%covInd( input%maxSize, num_indiv))
      
    do j = 1, num_indiv
      do i =1, size(ind_values(:,1))
        if (passed(j) == ind_values(1, i)) then
          dataIn%covInd(:,j) = ind_values(:,i)
        end if
      end do
    end do
    deallocate(ind_values)

    end subroutine testData





  logical function checkDataInteger(matrixIn, maxSize) result (out)
    integer, intent(in)::maxSize
    integer, allocatable, dimension(:,:)::matrixIn
!    logical, intent(out):: out

    integer:: counter

    integer, allocatable, dimension(:)::ID, hold
    out = .false.
    if ((size(matrixIn(:,1)).ne. maxSize).and. (size(matrixIn(:,1)).ne. 2)) then
      out = .true.
    else
      if (size(matrixIn(:,1)) .ne. maxSize) then
        allocate(hold(size(matrixIn(1,:))))
        allocate(ID(size(matrixIn(1,:))))

        ID=matrixIn(1,:)
        hold = matrixIn(2,:)

        deallocate(matrixIn)
        allocate(matrixIn(maxSize,size(ID)))
        matrixIn(1,:) = ID
        do counter = 2, maxSize
          matrixIn(counter,:) = hold
        end do
      end if
    end if
 end function checkDataInteger


  logical function checkData(matrixIn, maxSize) result (out)
    integer, intent(in)::maxSize
    real, allocatable, dimension(:,:)::matrixIn
!    logical, intent(out):: out

    integer:: counter

    integer, allocatable, dimension(:)::ID, hold
    out = .false.
    if ((size(matrixIn(:,1)).ne. maxSize).and. (size(matrixIn(:,1)).ne. 2)) then
      out = .true.
    else
      if (size(matrixIn(:,1)) .ne. maxSize) then
        allocate(hold(size(matrixIn(1,:))))
        allocate(ID(size(matrixIn(1,:))))

        ID=matrixIn(1,:)
        hold = matrixIn(2,:)

        deallocate(matrixIn)
        allocate(matrixIn(maxSize,size(ID)))
        matrixIn(1,:) = ID
        do counter = 2, maxSize
          matrixIn(counter,:) = hold
        end do
      end if
    end if
 end function checkData

  subroutine readInFileReal(arrayIn, filename)
    real, allocatable, dimension(:,:), intent(out)::arrayIn
    character(len=*)::filename

    integer::num_indiv, IOstatus, nRow, i
    character(len=2200000)::temp
    logical::test

    IOStatus = 0
      open(100, file = filename, status = "old", action = "read")
      num_indiv=-1
      do while (IOstatus == 0) 
        read(100, "(A)", IOstat = IOstatus) temp
        num_indiv = num_indiv +1
      end do

      if (Iostatus>0) then
        write(*,"(A,A)") "There was some problem with reading the file:", filename
        stop
      end if

      rewind(100)
      read(100, "(A)") temp
      nRow = 0
      if (temp(1:1) .ne. " ") then
        test = .true.
      else
        test = .false.
      end if

      do i = 1, len(trim(temp))
        if (temp(i:i) .ne. " ") then
          if (test) then 
            nRow = nRow+1
          end if
          test = .false.
        else
          test = .true.
        end if
      end do

      allocate(arrayIn(nRow, num_indiv))
      rewind(100)
      do i = 1, num_indiv
        read(100, *) arrayIn(:,i)
      end do
      close(100)
      !write(*,*) nRow, counting , "NROW", filename


 end subroutine readInFileReal

  subroutine readInFileInteger(arrayIn, filename)
    integer, allocatable, dimension(:,:), intent(out)::arrayIn
    character(len=*)::filename

    integer::num_indiv, IOstatus, nRow, i,j
    character(len=2200000)::temp
    logical::test

    IOStatus = 0
      open(100, file = filename, status = "old", action = "read")
      num_indiv=-1
      do while (IOstatus == 0) 
        read(100, "(A)", IOstat = IOstatus) temp
        num_indiv = num_indiv +1
      end do

      if (Iostatus>0) then
        write(*,"(A,A)") "There was some problem with reading the file:", filename
        stop
      end if

      rewind(100)
      read(100, "(A)") temp
      nRow = 0
      if (temp(1:1) .ne. " ") then
        test = .true.
      else
        test = .false.
      end if

      do i = 1, len(trim(temp))
        if (temp(i:i) .ne. " ") then
          if (test) then 
            nRow = nRow+1
          end if
          test = .false.
        else
          test = .true.
        end if
      end do

      
      allocate(arrayIn(nRow, num_indiv))

      rewind(100)
      do i =1, num_indiv
        read(100, *) arrayIn(:, i)
      end do

      close(100)
 end subroutine readInFileInteger
end module readInData

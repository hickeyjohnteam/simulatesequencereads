Module ReadInInput
  use global
  implicit none

  contains

  subroutine readInput(dataIn)
    !Written by Diarmaid de Burca, May 2016
    !Completed
    implicit none

    type(varInput)::dataIn
    integer:: nLoci, nInd
    integer, allocatable, dimension(:)::cov,varCovInd, varCovLoc
    logical::imputeMissingDosages, imputeTrueAlleleFreq,NAndR


    character(len=1000):: optionName, temp, OptionValue 
    character(len=:), allocatable::genoFile, covIndFile, covLocFile
    character(len = 1000), dimension(:), allocatable::tempChar

    integer::tempInt,  i, beginning, ending, counting
    integer::IOStatus
    real(kind=8), dimension(:), allocatable::error
    character::c

    open(100, file = "SimulateSeqReadsSpec.txt", status = "old", action = "read")
    IOStatus = 0
    do while (IOStatus == 0)
      read(100, *, iostat=IOStatus) optionName, optionValue
!      write(*,*) trim(optionName), " ", trim(optionValue)

      select case(TLC(trim(optionName)))
        case("genofile")
          dataIn%genoFile = trim(optionValue);

        case("nloc") 
          tempInt=0
          read(OptionValue, *) tempInt
          dataIn%nLoci = tempInt

        case ("nind")
          read(optionValue,*) dataIn%nInd

        case ("covindfile")
          dataIn%covIndFile = trim(optionValue)

        case("covlocfile")
          dataIn%covLocFile = trim(optionValue)

        case("cov")
          backspace(100)
          read(100, "(A)") optionValue
          counting = 0 
          do i = 1, len(trim(optionValue))
            c = optionValue(i:i)
            if (c==",") then
              counting = counting +1
            end if
          end do
          if (allocated(dataIn%cov)) then
            deallocate(dataIn%cov)
          end if
          allocate(dataIn%cov(counting))
          read(optionValue, *) temp, dataIn%cov

        case("varcovind")
          backspace(100)
          read(100, "(A)") optionValue
          counting = 0
          do i =1, len(trim(optionValue))
            c = optionValue(i:i)
            if (c==",") then
              counting = counting+1
            end if
          end do
          allocate(dataIn%varCovInd( counting))

          read(optionValue, *) temp, dataIn%varCovInd

        case("varcovloc")
          backspace(100)
          read(100, "(A)") optionValue
          counting = 0
          do i =1, len(trim(optionValue))
            c = optionValue(i:i)
            if (c==",") then
              counting = counting+1
            end if
          end do
          allocate(dataIn%varCovLoc( counting))

          read(optionValue, *) temp, dataIn%varCovLoc

        case("error")
          backspace(100)
          read(100, "(A)") optionValue
          counting = 0
          do i =1, len(trim(optionValue))
            c = optionValue(i:i)
            if (c==",") then
              counting = counting+1
            end if
          end do
          allocate(dataIn%error( counting))

          read(optionValue, *) temp,  dataIn%error

        case("imputemissingdosages")
          backspace(100)
          read(100, "(A)") optionValue
          counting = 0
          do i =1, len(trim(optionValue))
            c = optionValue(i:i)
            if (c==",") then
              counting = counting+1
            end if
          end do
          if (allocated(tempChar)) then 
            deallocate(tempChar)
          end if
          allocate(tempChar( counting+1))
          allocate(dataIn%imputeMissingDosages(counting))

          read(optionValue, *)  tempChar 
          do i = 1, counting
            if (TLC(trim(tempChar(i+1)))=="yes") then
              dataIn%imputeMissingDosages(i)=.true.
            else
              dataIn%imputeMissingDosages(i)=.false.
            end if
          end do
          deallocate(tempChar)

        case("imputetrueallelefreq")
          backspace(100)
          read(100, "(A)") optionValue
          counting = 0
          do i =1, len(trim(optionValue))
            c = optionValue(i:i)
            if (c==",") then
              counting = counting+1
            end if
          end do
          if (allocated(tempChar)) then 
            deallocate(tempChar)
          end if
          allocate(tempChar( counting+1))
          allocate(dataIn%imputeTrueAlleleFreq(counting))

          read(optionValue, *)  tempChar 
          do i = 1, counting
            if (TLC(trim(tempChar(i+1)))=="yes") then
              dataIn%imputeTrueAlleleFreq(i)=.true.
            else
              dataIn%imputeTrueAlleleFreq(i)=.false.
            end if
          end do
          deallocate(tempChar)

        case("nandr")
          backspace(100)
          read(100, "(A)") optionValue
          counting = 0
          do i =1, len(trim(optionValue))
            c = optionValue(i:i)
            if (c==",") then
              counting = counting+1
            end if
          end do
          if (allocated(tempChar)) then 
            deallocate(tempChar)
          end if
          allocate(tempChar( counting+1))
          allocate(dataIn%NAndR(counting))

          read(optionValue, *)  tempChar 
          do i = 1, counting
            if (TLC(trim(tempChar(i+1)))=="yes") then
              dataIn%NAndR(i)=.true.
            else
              dataIn%NAndR(i)=.false.
            end if
          end do
          deallocate(tempChar)

        case("output")
          dataIn%output = trim(optionValue)

        case("selfile")
          dataIn%selFile = trim(optionValue)

        case("splitbyloc")
          read(optionValue, *) dataIn%splitLoc

        case default
          write(*,"(A,A)") trim(optionName), ": not a valid option - perhaps something is misspelt?"
        end select
      end do

    if (dataIn%splitLoc>0) then
      if (mod(dataIn%nLoci, dataIn%splitLoc) .ne. 0) then
        write(*,*) "nLoc/SplitbyLoc should have no remainder"
        stop
      end if
    else if (dataIn%splitLoc <0) then
      dataIn%splitLoc = 0
    end if
    !write(*,*) "Split Loc: ", dataIn%splitLoc

    dataIn%maxSize = 1+max(size(dataIn%imputeMissingDosages), size(dataIn%imputeTrueAlleleFreq), size(dataIn%cov), size(dataIn%varCovInd), size(dataIn%varCovLoc), size(dataIn%error))
    close(100)

  end subroutine readInput

  subroutine checkInput(input, dataIn)
    use ReadInData
    type(varInput)::input
    type(dataInput) dataIn

    integer::i
    real, allocatable, dimension(:)::temp_real
    integer, allocatable, dimension(:)::temp_int
    integer::a
    real::b
    logical::l

     i = max(size(input%imputeMissingDosages), size(input%imputeTrueAlleleFreq), size(input%cov), size(input%varCovInd), size(input%varCovLoc), size(input%error))
     if (input%maxSize>(i+1)) then
       i = input%maxSize-1
     else
       input%maxSize=i+1
     end if

     if (checkData(dataIn%covInd, input%maxSize)) then
        write(*,*) "CovInd"
        write(*,*) input%maxSize
        write(*,*) dataIn%covInd
        stop
      end if

      if (checkData(dataIn%covLoc, input%maxSize)) then
        write(*,*) "CovLoc"
        write(*,*) dataIn%CovLoc
        stop
      end if
      
      if (checkDataInteger(dataIn%sel, input%maxSize)) then
        write(*,*) input%selFile
      end if


    if (.not.((size(input%imputeMissingDosages).eq. 1) .or. (size(input%imputeMissingDosages) .eq. i))) then
      write(*,*) "I'm sorry, but you need to have an option for every scenario or a single option constant over all scenarios for imputing Missing dosages."
      stop
    end if
    if (.not.((size(input%imputeTrueAlleleFreq).eq. 1) .or. (size(input%imputeTrueAlleleFreq) .eq. i))) then
      write(*,*) "I'm sorry, but you need to have an option for every scenario or a single option constant over all scenarios for imputing True Allele Frequencies."
      stop
    end if

    if (.not.((size(input%NAndR).eq. 1) .or. (size(input%NAndR) .eq. i))) then
      write(*,*) "I'm sorry, but you need to have an option for every scenario or a single option constant over all scenarios for NAndR"
      stop
    end if

    if (TLC(input%covIndFile).eq."none") then

      if (.not.((size(input%cov).eq. 1) .or. (size(input%cov) .eq. i))) then
        write(*,*) "I'm sorry, but you need to have an option for every scenario or a single option constant over all scenarios average coverage (cov)"
        stop
      end if

      if (.not.((size(input%varCovInd).eq. 1) .or. (size(input%varCovInd) .eq. i))) then
        write(*,*) "I'm sorry, but you need to have an option for every scenario or a single option constant over all scenarios for average coverage variation (varCovInd)"
        stop
      end if
    end if

    if (TLC(input%covLocFile) == "none") then

      if (.not.((size(input%varCovInd).eq. 1) .or. (size(input%varCovInd) .eq. i))) then
        write(*,*) "I'm sorry, but you need to have an option for every scenario or a single option constant over all scenarios for average Loci variation (varCovLoc)"
        stop
      end if
    end if

    if (.not.((size(input%error).eq. 1) .or. (size(input%error) .eq. i))) then
      write(*,*) "I'm sorry, but you need to have an option for every scenario or a single option constant over all scenarios for the error"
      stop
    end if

    if (size(input%imputeMissingDosages) ==1) then
      l = input%imputeMissingDosages(1)
      deallocate(input%imputeMissingDosages)
      allocate(input%imputeMissingDosages(i))
      input%imputeMissingDosages = l
    end if

    if (size(input%imputeTrueAlleleFreq)==1) then
      l = input%imputeTrueAlleleFreq(1)
      deallocate(input%imputeTrueAlleleFreq)
      allocate(input%imputeTrueAlleleFreq(i))
      input%imputeTrueAlleleFreq = l
    end if

    if (size(input%NAndR)==1) then
      l = input%NAndR(1)
      deallocate(input%NAndR)
      allocate(input%NAndR(i))
      input%NAndR = l
    end if

    if (size(input%cov)==1) then
      a = input%cov(1)
      deallocate(input%cov)
      allocate(input%cov(i))
      input%cov = a
    end if

    if (size(input%varCovInd) ==1) then
      a = input%varCovInd(1)
      deallocate(input%varCovInd)
      allocate(input%varCovInd(i))
      input%varCovInd = a
    end if

    if (size(input%varCovLoc)==1) then
      a = input%varCovLoc(1)
      deallocate(input%varCovLoc)
      allocate(input%varCovLoc(i))
      input%varCovLoc = a
    end if

    if (size(input%error)==1) then
      b=input%error(1)
      deallocate(input%error)
      allocate(input%error(i))
      input%error = b
    end if

 end subroutine checkInput

end module ReadInInput

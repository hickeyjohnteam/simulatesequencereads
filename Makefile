# General variables
NAME:=SimulateSequenceReads
VERSION:= $(shell git rev-parse --short HEAD)
SUBVERSION:=0
PROGRAM:=$(NAME)$(VERSION)

# Set the default compiler to iFort
FC:=ifort
FFLAGS:=-O3 -DVERS=""commit-$(VERSION)""

# MS Windows
ifeq ($(OS), Windows_NT)
	SRCDIR      := src/
	BUILDDIR    := 
	TARGETDIR   := 
	OSFLAG := "OS_WIN"
	FFLAGS := $(FFLAGS) /static /i8 /fpp  /Qmkl /D $(OSFLAG)
	ABOPT := -static  -Qmkl
	obj := .obj

	MAKEDIR :=
	exe := .exe 
	CC := cl
	CFLAGS := /EHsc

	DEL := del
else
	# Linux or Mac OSX
	SRCDIR      :=src/
	BUILDDIR    := objs/
	TARGETDIR   := bin/
	obj := .o
	OSFLAG := "OS_UNIX"
	ABOPT := 
	exe :=
	FFLAGS:= $(FFLAGS) -i8 -static_intel -fpp -module $(BUILDDIR) -D $(OSFLAG)
	uname := $(shell uname)
	MAKEDIR := @mkdir -p 
	DEL := rm -rf
  # Linux only
	ifeq ($(uname), Linux)		
		FFLAGS := $(FFLAGS) -static-libgcc -static-libstdc++ # Removed '-static'
	endif
endif

SRC_ABS = $(SRCDIR)AlphaHouseMod.f90 $(SRCDIR)Global.f90 \
			  	$(SRCDIR)Random.f90 \
			  	$(SRCDIR)ReadData.f90 \
			  	$(SRCDIR)ReadInput.f90 \
					$(SRCDIR)WriteData.f90 \
			  	$(SRCDIR)SimSeqReadsv2.f90 


all: directories compile
	

compile: 
	$(FC) -o $(TARGETDIR)$(NAME) $(SRC_ABS) $(FFLAGS)

directories:
	$(MAKEDIR)  $(TARGETDIR)
	$(MAKEDIR)  $(BUILDDIR)

debug: FFLAGS:= -i8 -traceback -g -D VERS=""commit-$(VERSION)"" -D $(OSFLAG) -debug all -warn -check bounds -check format \
		-check output_conversion -check pointers -check uninit -fpp -module $(BUILDDIR)

debug: all 

clean: 
	rm *.mod $(NAME) $(NAME)_debug 
	

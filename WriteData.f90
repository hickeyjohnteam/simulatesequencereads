module writeoutdata
  use global

  implicit none
  contains

    subroutine writeOutput(input, sequencability, seq, dos, i)
      implicit none
      type(varInput)::input

      real, allocatable, dimension(:,:), intent(inout):: dos
      integer, allocatable, dimension(:,:), intent(inout)::seq
      real, allocatable, dimension(:,:), intent(inout):: sequencability
      integer, intent(in)::i
      integer:: j, k, l
      integer:: selection
      logical sel
      character(1000)::basefilename, seqfilename, dosfilename
      character(30):: tempError

      write(baseFilename, "(A,A,I0)") input%genoFile, "_Scenario_", i

      if (TLC(input%covIndFile)=="none") then
        write(baseFileName, "(A,A,I0,A,I0)") trim(baseFileName), "_CoverageIndMean_", input%cov(i), "_CoverageIndVar_", input%varCovInd(i)
      end if

      if (TLC(input%covLocFile)=="none") then
        write(baseFileName, "(A,A,f0.0)") trim(basefileName), "_CoverageLocGamma_", input%varCovLoc(i)
      end if

      write(tempError, "(f14.4)") input%Error(i)
      write(baseFileName, "(A,A,A)") trim(baseFileName), "_Error_", trim(adjustl(tempError))!,"_Select_", sel

      write(seqFileName,"(A,A,L1)") trim(baseFileName), "_Reads_NAndR_", input%nAndR(i)
      write(dosFilename, "(A,A,L1,A,L1,A)") trim(baseFileName), "_Dosage_ImputeMissintDosages_",input%imputeMissingDosages(i),"_imputeKnownAlleleFrequency_", input%imputeTrueAlleleFreq(i),".txt"

      open(100, file = trim(seqFileName), action = "write", status ="replace")
      open(101, file=trim(dosFileName), action="write", status="replace")
      do k = 1, size(seq(:,1))
        do j =1, size(seq(1,:))
          write(100, "(I0,A)", advance="No") seq(k, j), " "
        end do
        write(100,*)
      end do

      do k = 1, size(dos(:,1))
        do j = 1, size(dos(1,:))
          write(101, "(f20.5)", advance="no") dos(k, j)
        end do
        write(101, *) 
      end do

      close(100)
      close(101)
      write(baseFileName, "(A,A, I0, A)") input%genoFile, "_Scenario_", i, "_sequencability_CoverageLogGamma_"

      if (TLC(input%covLocFile) =="none") then
        write(baseFileName, "(A, I0.0,A)") trim(baseFileName), input%VarCovLoc(i), ".txt"
      else
        write(baseFileName, "(A, A,A)") trim(baseFileName),"_Predefined_", ".txt"
      end if
      open(103, file=trim(baseFileName), action="write", status="replace")
      do k = 1, size(sequencability(1,:))
        write(103, *) sequencability(1,k), sequencability(i+1,k)
      end do
      close(103)

      
      end subroutine writeOutput

end module writeoutdata

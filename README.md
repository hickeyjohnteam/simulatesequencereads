# README #

### What is this repository for? ###

* Sequence Reads Simulator
* Main Developer: [Diarmaid De Burca](diarmaid.deburca@ed.ac.uk)
* Most updated branch: XXX

### Description ###

Fortan Simulator for Sequence Data. Heavily used by [Mara Battagin](mara.battagin@roslin.ed.ac.uk)

### Parameters File ###

The parameters file name must be "SimulateSeqReadsSpec.txt".   It has 15 options:

Genofile - Sets the name of the file holding the genotype information

nLoc - Sets the number of Loci

nInd - sets the number of individuals

CovIndFile - Sets the file holding coverage information for the individuals. Set to none to geterate covariances

CovLocFile - Sets the file holding covarage information for the Loci. Set to none to geterate covariances

Cov - Set the covariace parameter

VarCovInd - Set the variance of the individuals to simulate (ignored when file option is set to non-none)

VarCovLoc - Set the variance of the loci to simulate

Error - Set the read error rate

ImputeMissingDosages - Should we impute the missing dosages and store them"? Set to Yes/No

ImputeTrueAlleleFreq - Should we impute the true allele frequency? Set to Yes/No

NAndR - Set how the (missing) dosage/reads are calculated

OutPut - Base file path to store output.   Output files are generated by appending the name of the data to the file.

SelFile - File holding the ids of the animals to sequence

SplitByLoc - This doesn't do anything

program SimulateSeqReads
  use random
  use global
  use readInInput
  use ReadInData
  use writeoutdata
  implicit none

  !type genMat
   ! integer, dimension (:,:), allocatable::trueGeno
   ! integer, dimension(:), allocatable::ID
  !end type genMat


  !type(genMat):: tes
  type(outp)::dataOut
  type(varInput)::inputVar
  type(dataInput)::dataIn
  real(kind=4), dimension(2):: normdist, gammadist
  integer::i, j

  call seed_random_number(1024)
!  call RANDOM_SEED(PUT=4)


  call readInput(inputVar)

  call readData(inputVar, dataIn)

  call MakeArray(dataIn, inputVar)

  call checkInput(inputVar, dataIn)

  call testData(dataIn, inputVar)

  call simulateSequence(dataIn, inputVar)

contains


  subroutine simulateSequence(dataIn, input)
    use Global
    use, intrinsic :: ieee_arithmetic
    implicit none
    type(dataInput)::dataIn
    type(varInput)::input

    integer::nInd, nLoci, i, j, k, l,nScenario
    real, allocatable, dimension(:)::sequencability, Means, temp
    real, allocatable, dimension(:,:):: dos
    integer, allocatable, dimension(:)::n
    integer, allocatable, dimension(:,:)::seq

    integer::g, counter
    real::sumtemp

    nInd = size(dataIn%geno(1,:))
    nLoci = size(dataIn%geno(:,1))-1
    nScenario = input%maxSize -1 !size(input%imputeMissingDosages)

    allocate(seq(nInd*2, nLoci+1))
    allocate(dos(nInd, NLoci+1))
    allocate(n(nLoci))

    allocate(Means(nLoci))
    allocate(temp(nInd))

    do k = 1, nScenario 
      dos=0
      seq=0
      do i=1, 2*nInd,2
 
        seq(i, 1) = dataIn%geno(1,(i+1)*0.5)
        seq(i+1, 1) = dataIn%geno(1,(i+1)*0.5)
        dos(0.5*(i+1), 1) = dataIn%geno(1,(i+1)*0.5)

        do j =2, nLoci+1
          sumtemp =  dataIn%covInd(k+1, 0.5*(i+1))* dataIn%CovLoc(k+1, j-1)
          n(j-1) = random_poisson(sumtemp, .true.)
        end do

        seq(i, 2:nLoci+1) = n(:)


        do j=2, nLoci+1
          if (seq(i, j) ==0) then
            seq(i+1,j) = 0
          else
            g = dataIn%geno(j, 0.5*(i+1))
            if (g==0) then
              seq(i+1,j) =random_binomial2(n(j-1), input%Error(k), .true.) 
            else if (g==1) then
              seq(i+1,j) = random_binomial2(n(j-1), 0.5, .true.)
            else if (g==2) then
              seq(i+1,j) = random_binomial2(n(j-1), 1-input%Error(k), .true.)
            else !g ==9
              seq(i+1,j) = 0
              seq(i,j) = 0
            end if
          end if
        end do
!
        if (input%NAndR(k)==.false.) then
          do j=1, nLoci
            seq(i, j+1) = seq(i, j+1) - seq(i+1, j+1)
          end do
        end if

        if (input%NAndR(k) ==.true.) then
          do j = 2, nLoci+1
            if (seq(i, j) .ne. 0) then
              dos(0.5*(i+1),j)= 2.0*seq(i+1,j)/seq(i, j)
            else
              dos(0.5*(i+1),j) = ieee_value(dos(0.5*(i+1), j), ieee_quiet_nan)
            end if
          end do
        else
          do j=2, nLoci+1
            if (seq(i, j).ne. 0 .or. seq(i+1, j).ne. 0) then
              dos(0.5*(i+1),j)= 2.0*seq(i+1, j)/(seq(i,j) + seq(i+1,j))
            else
              dos(0.5*(i+1),j) = ieee_value(dos(0.5*(i+1), j), ieee_quiet_nan)
            end if
          end do
        end if
      end do

      if (input%imputeMissingDosages(k)) then
        if (input%imputeTrueAlleleFreq(k)) then
          do l=2,nLoci+1
            temp = dataIn%geno(l,:)
            sumtemp = 0
            counter = 0
            do j = 1, size(temp)
              if (temp(j) .ne. 9) then
                sumtemp = sumtemp+temp(j)
              else
                counter = counter +1
              end if
            end do
            sumtemp = sumtemp/(size(temp)-counter)
            Means(l-1) = sumtemp
          end do
        else
          do l=2, nLoci+1
            temp = dos(:, l)
            sumtemp =0
            do j=1, size(temp)
              sumtemp = sumtemp +temp(j)
            end do
            sumtemp = sumtemp/size(temp)
            Means(l-1) = sumtemp
          end do
        end if

        do l = 1, nLoci
          do j = 1, nInd
            if (isnan(dos(j,l+1))) then
              dos(j,l+1) = Means(l)
            end if
          end do
        end do
      end if

      call writeOutput(input, dataIn%covLoc, seq, dos, k)

    end do
    deallocate(Means)
    deallocate(temp)

  end subroutine simulateSequence

end program SimulateSeqReads

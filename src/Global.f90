module global
use iso_fortran_env
  implicit none
  type varInput
    integer::nLoci, nInd, splitLoc, maxsize
    logical, dimension(:), allocatable:: imputeMissingDosages, imputeTrueAlleleFreq, NAndR
    character(len=:), allocatable:: genoFile, covIndFile,covLocFile,selFile, output
    real(kind=int64), allocatable, dimension(:)::cov,varCovInd, varCovLoc
    real(kind=int64), dimension(:), allocatable::error
  end type varInput

  type dataInput
    integer, dimension(:,:), allocatable:: geno, sel   !Geno holds the animals genotype.   It is an array with (number of individuals) x (number of Loci +1). The first row is the individual ID. 
    !Sel holds
    real(kind=int64), dimension(:,:), allocatable::covInd,covLoc
    integer::nCov
  end type dataInput


  contains

   function TLC(str)
       character(*), intent(in) :: str
       character(len=512) :: TLC
       integer :: i
       TLC = trim(str)
       do i = 1, len(TLC)
           select case(TLC(i:i))
               case("A":"Z")
                   TLC(i:i) = achar(iachar(TLC(i:i))+32)
           end select
       end do
       return
   end function TLC
end module global

module simseqreads
  implicit none

  type input
    character(len=:), allocatable::genoFile, CovIndFile, covLocFile, SelFile, Output
    integer:: nLoc, nInd, cov, varCovInd, varCovLoc, splitByLoc
    real(kind = 8):: error
    logical:: imputeMissingDosages, imputeTrueAlleleFreq, NAndR
    integer, dimension(:,:), allocatable:: covInd, genotype,covLoc
  end type input

    
!  input:: varIn


contains


  subroutine SimSpecReads()
    type(input):: inputOptions

    inputOptions = readIn()

    write(*,*) inputOptions%genofile

  end subroutine SimSpecReads


  subroutine checkFile(input) 
    character(len=*)::input
    logical::success
    
    Inquire(file = input, exist = success)
    if (.not. success) then
      write(*,"(A)") input, " must exits!"
      stop
    end if
  end subroutine checkFile

  function ReadIn() result (varOut)
    type(input)::varOut
    character(len=100)::temp, temp2
    character(5):: logical
    logical:: success
    integer::i

    call checkFile("SimulateSeqReadsSpec.txt")
    open (100, file="SimulateSeqReadsSpec.txt", status = "old", action = "read")
    

    read(100, * ) temp, temp2
    varOut%genoFile = trim(temp2)
    call checkFile(varOut%genoFile)
    call readArrayInt(varOut%genoFile, varOut%genotype)
      
    read(100, *) temp, varOut%nLoc
    read(100, *) temp, varOut%nInd
    read(100, *) temp, temp2
    varOut%covIndFile=trim(temp2)
    if (varOut%covIndFile .ne. "None") then
      call CheckFile(varOut%covIndFile)
      call readArrayInt(varOut%covIndFile, varOut%covInd)
    end if

    read(100, *) temp, temp2
    varOut%covLocFile=trim(temp2)
    if (varOut%covLocFile .ne. "None") then
      call checkFile(varOut%covLocFile)
      call ReadArrayInt(varOut%covLocFile, VarOut%covLoc)
    end if

    
    read(100, *) temp, varOut%cov
    read(100, *) temp, varOut%varCovInd
    if (varOut%covIndFile .ne. "None") then
      varOut%cov = -9
      varOut%varCovInd = -9
    end if

    read(100, *) temp, varOut%varCovLoc
    read(100, *) temp, varOut%error
    read(100, *) temp, logical
    varOut%imputeMissingDosages = getBoolean(logical)
    read(100, *) temp, logical
    varOut%imputeTrueAlleleFreq = getBoolean(logical)
    read(100,*) temp, logical
    varOut%NAndR = getBoolean(logical)
    read(100,*) temp, temp2
    varOut%Output=trim(temp2)
    read(100,*) temp, temp2
    varOut%SelFile=trim(temp2)
    read(100, *) temp, varOut%splitByLoc

    close(100)

    !Now do the required checks
  end function ReadIn

  subroutine readArrayInt (input,output) 
    integer, dimension(:,:), allocatable::output
    
    integer:: nRow, nCol, tempInt
    character(len=*):: input
    character(1000)::temp
    character::c
    integer:: IOstatus
    integer::i
    logical::lastC


    open(201, file = trim(input), status="old", action = "read")

    IOstatus = 0
    nRow = -1
    do while (IOstatus == 0)
      read(201, *, IOSTAT = IOstatus) temp
      nRow = nRow+1
    end do
    if (IOstatus .gt. 0) then
      write(*,*) "Reading the input from ", input , " cannot be trusted!"
      stop
    end if
    rewind(201)
    IOstatus = 0
    nCol = 0
    read(201, "(A)") temp
    do i=1, len(trim(temp))
      c = temp(i:i)
      if (ichar(c)==9 .or. c==" ") then
        lastC = .false.
      else
        if (lastC == .false.) then
          nCol = nCol+1
          lastC = .true.
        end if
      end if
    end do
    write(*,*) nCol

    allocate(output(nRow, nCol))
    rewind(201)
    read(201, *) output
    close(201)

  end subroutine readArrayInt
  
  function getBoolean(input) result (boolout)
    character(len=*), intent(in) :: input
    logical::boolout

    if ((trim(input)=="Yes") .or. (trim(input)=="yes")) then
      boolout = .true.
    else
      boolout = .false.
    end if
  end function getBoolean

  subroutine simulateSeqReadsForAlpha(genIn)
    implicit none

    integer, dimension(:,:), allocatable:: genIn

    integer:: nRow, nCol

    rRow = size(genIn(1,:))
    nCol = size(genIn(:,1))



    end subroutine simulateSeqReadsForAlpha
end module simseqreads
